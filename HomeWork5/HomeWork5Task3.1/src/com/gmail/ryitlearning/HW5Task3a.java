package com.gmail.ryitlearning;

import java.util.Arrays;

public class HW5Task3a {

	public static void main(String[] args) {
		
		/**
		 * 3) Создать массив случайных чисел (размером 15 элементов). Создайте второй
		 * массив в два раза больше, первые 15 элементов должны быть равны элементам
		 * первого массива, а остальные элементы заполнить удвоенных значением
		 * начальных. Например Было → {1,4,7,2} Стало → {1,4,7,2,2,8,14,4}
		 * 
		 */

		int[] a = new int[15];
		
		for (int i = 0; i < a.length; i++) {
			a[i] = (int) (Math.random()*10);
		}
		System.out.println(Arrays.toString(a));
		
		int[] b = new int[a.length * 2];
		
		b = Arrays.copyOfRange(a, 0, a.length * 2);
		
		
		for (int j = a.length; j < b.length; j++) {
			b[j] = b[j - a.length] * 2;
		}
		System.out.println(Arrays.toString(b));		

	}

}
