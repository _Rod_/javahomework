package com.gmail.ryitlearning;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		/**
		 * 2) ���� ������������� ���, � ������� 4 ��������. ����� �������� ���������� �
		 * �������. �� ����� ����� 4 ��������. �������� ��������� ������� ������� �����
		 * �������� � ����������, � ������� �� ����� �� ����� �����, ������ ��������
		 * ������������ ��� ��������. ���� ����� �������� ��� � ���� ���� �� �����
		 * �������� �� ���� ������������.
		 */

		int aptNbr;
		System.out.println("For information input  required appartment number and press Enter");
		aptNbr = sc.nextInt();

		int entranceNumber;
		entranceNumber = 0;

		int floorNumber;
		floorNumber = 0;

		String error;
		error = "Invalid input or requested flat is not existed";

		if (aptNbr > 0 && aptNbr <= 4 * 9 * 4) {
			entranceNumber = (aptNbr - 1) / 36 + 1;
			aptNbr = aptNbr - 36 * (entranceNumber - 1);
			floorNumber = (aptNbr - 1) / 4 + 1;
			System.out.println("The requested appartment is at entrance #"+ entranceNumber + ", on floor #" + floorNumber);
		} else {
			System.out.println(error);
		}
		sc.close();

	}

}
