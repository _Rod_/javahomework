package com.gmail.ryitlearning;

import java.util.Arrays;

public class MainHW7Task2 {

	public static void main(String[] args) {
		/**
		 * 2) �������� ���� ������� ������ Arrays.toString() ��� int[].
		 */

		int[] array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		
		System.out.println(Arrays.toString(array) + " - Standart method");
		System.out.println(myArrayToString(array) + " - Self-made method");

	}

	public static String myArrayToString(int[] a) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for (int i = 0; i < a.length - 1; i++) {
			sb.append(a[i] + "; ");
		}
		if (a.length != 0) {
			sb.append(a[a.length - 1]);
		}
		sb.append("}");
		return sb.toString();

	}
}
