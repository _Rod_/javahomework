package com.gmail.ryitlearning;

import java.util.Scanner;

public class MainHW5Task4 {

	public static void main(String[] args) {
		/**
		 * 4) ������� ������ ������ � ���������� � ���������� ��������� ��� �����������
		 * �������� ���������� ������� � 'b' � ���� ������, � ������� ��������� ��
		 * �����.
		 */
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Input text string and press Enter");
		String textLine = sc.nextLine();

		int num = 0;

		
		char[] symbols = textLine.toCharArray();
		for (int i = 0; i < symbols.length; i++) {
			if (symbols[i] == 'b') {
				num = num + 1;
			}
		}
		System.out.println("The number of" + " 'b' " + "symbol in text line is: " + num);
		sc.close();
	}

}
