package com.gmail.ryitlearning;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		// HW#1 task_1:
		/**
		 * 1) �������� ��������� ������� ��������� 5-� ������� ����� � ���������� �
		 * ������� ����� �� �������� ��� �������. �������� : ����������� ����� 54698
		 * ����������: 5 4 6 9 8
		 */

		int number;
		System.out.println("Input 5-digit integer number and press Enter");
		number = sc.nextInt();
		
		int digit1 = number / 10000;
		System.out.println("1-st digit is - " + digit1 + ";");
		
		int digit2 = number % 10000 / 1000;
		System.out.println("2-nd digit is - " + digit2 + ";");
		
		int digit3 = number % 1000 / 100;
		System.out.println("3-rd digit is - " + digit3 + ";");
		
		int digit4 = number % 100 / 10;
		System.out.println("4-th digit is - " + digit4 + ";");
		
		int digit5 = number % 10 / 1;
		System.out.println("5-th digit is - " + digit5 + ".");

		
		// HW#2 task_2:
		/** 2)�������� ��������� ������� �������� � ������� �� ����� 
			  ������� ������������ ���� �������� ��� �������. */
		
		double sideA;
		System.out.println("Input length of side A (cm) and press Enter");
		sideA = sc.nextDouble();
		
		double sideB;
		System.out.println("Input length of side B (cm) and press Enter");
		sideB = sc.nextDouble();
		
		double sideC;
		System.out.println("Input length of side C (cm) and press Enter");
		sideC = sc.nextDouble();
		
		double p;
		p = (sideA + sideB + sideC) / 2;
		
		double triangleArea;
		triangleArea = Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
		
		System.out.println("The area of a triangle is: " + triangleArea + " cm2");
		
		
		//HW#2 task_3:
		/** 3)�������� ��������� ������� �������� � ������� �� ����� 
			  ������ ����������, ���� �� ������ ����������� � ����������. */
		
		
		double r;
		System.out.println("Input a Radius of a circle (cm) and press Enter");
		r = sc.nextDouble();
		
		double s;
		s = 2 * Math.PI * r;
		System.out.println("Considering the Radius = " + r + " cm, �ircumference = " + s + " cm");
	}

}
