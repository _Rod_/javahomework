package com.gmail.ryitlearning;

public class MainHw6Task4 {

	public static void main(String[] args) {
		/**
		 * 4) �������� ����� ������� ��������� �������� ����� �������� � ������� �����
		 * �����. ���� ����� ������� � ������� ���� �� ������� ��� ������, ���� ��� ��
		 * ����� ������ ���������� ����� - �-1�
		 */

		int[] a = new int[] { 1, 5, -11, 89, 6, 72, 10, 54, 23 };

		System.out.println("The index of requested element of array is: " + findIndex(23, a));

	}

	public static int findIndex(int element, int[] a) {
		int elementIndex = -1;
		for (int i = 0; i < a.length; i++) {
			if (a[i] == element) {
				elementIndex = i;
				break;
			}
		}
		return elementIndex;
	}

}
