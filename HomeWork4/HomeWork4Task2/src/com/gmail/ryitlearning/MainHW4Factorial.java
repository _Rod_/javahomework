package com.gmail.ryitlearning;

import java.util.Scanner;

public class MainHW4Factorial {

	public static void main(String[] args) {
		/**
		 * 2) ��������� � ������� ����� ��������� ����� - n ���������� � ����������
		 * (4<n<16). ��������� ����� ��� ������������ ���� ����� �� ����� ����� �� 1.
		 * �������� 5!=5*4*3*2*1=120
		 * 
		 */

		Scanner sc = new Scanner(System.in);

		int n;
		System.out.println(
				"To calculate the factorial of a number input n, in range from 4 to 16 (not including) and press Enter");
		n = sc.nextInt();

		int f = 1;

		if (n > 4 && n < 16) {

			for (int i = 1; i <= n; i++) {
				f = f * i;
			} System.out.print(n + "! = " + f);
		} else {
			System.out.println("Invalid input. The correct value is in range: 4..16");
		}
		
		
		
	}

}
